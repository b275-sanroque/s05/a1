<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    //established one side of the relationship
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    //established the many side of the relationship
    public function likes(){
        return $this->hasMany('App\Models\PostLike');
    }

    public function comments(){
        return $this->hasMany('App\Models\PostComment');
    }
}
